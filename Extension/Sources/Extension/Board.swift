//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 15/01/2024.
//

import Foundation
import Model



extension Board: CustomStringConvertible {
    public var description: String {
        var description = ""
        for row in grid {
            for cell in row {
                if let piece = cell.piece {
                    description += cell.cellType.symbol + piece.owner.symbol + piece.animal.symbol + " "}
                else {
                    description += cell.cellType.symbol + "   "}
            }
            description += "\n"
        }
        return description
    }
    
}
