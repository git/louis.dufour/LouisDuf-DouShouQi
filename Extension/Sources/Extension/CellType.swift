//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 15/01/2024.
//

import Foundation
import Model

public extension CellType {
    var symbol: String {
        switch self {
        case .unknown: return "   "
        case .jungle: return "🌿"
        case .water: return "💧"
        case .trap: return "🪤"
        case .den: return "🪹"
        }
    }
}
