//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 22/01/2024.
//

import Foundation

public struct VerySimpleRules: Rules {
    public var occurences: [Board : Int]
    public var historic: [Move]

    public init() {
        self.occurences = [Board: Int]()
        self.historic = [Move]()
    }
    
    public init(occurences: [Board: Int], historic: [Move]) {
        self.occurences = occurences
        self.historic = historic
    }
    
    public static func createBoard() -> Board {
        let initialBoardConfiguration: [[Cell]] = [
            // Ligne 1 - Joueur 1
            [Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .lion)),
             Cell(ofType: .den, ownedBy: Owner.player1), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .tiger)),
             Cell(ofType: .jungle)],

            // Ligne 2 - Joueur 1
            [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .rat)), Cell(ofType: .jungle),
             Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .cat)), Cell(ofType: .jungle),
             Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .elephant))],

            // Ligne 3 - Vide
            [Cell(ofType: .jungle), Cell(ofType: .jungle), Cell(ofType: .jungle),
             Cell(ofType: .jungle), Cell(ofType: .jungle)],

            // Ligne 4 - Joueur 2
            [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .elephant)), Cell(ofType: .jungle),
             Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .cat)), Cell(ofType: .jungle),
             Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .rat))],

            // Ligne 5 - Joueur 2
            [Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .tiger)),
             Cell(ofType: .den, ownedBy: Owner.player2), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .lion)),
             Cell(ofType: .jungle)]
        ]
        do {
           let board = try Board(withGrid: initialBoardConfiguration)
           return board
       } catch {
           print("Failed to initialize the board due to an error: \(error)")
           let defaultGrid: [[Cell]] = [[Cell(ofType: .jungle)]]
           return try! Board(withGrid: defaultGrid)
       }
    }
    
    
    public func getNextPlayer() -> Owner {
        // Implémentation basée sur l'historique des coups
        return historic.last?.owner == .player1 ? .player2 : .player1
    }
    
    public static func checkBoard(b: Board) throws {
        // Vérifier les dimensions du plateau
        guard b.nbRows == 5, b.nbColumns == 5 else {
            throw InvalidBoardError.badDimensions(b.nbRows, b.nbColumns)
        }
        
        // Vérifier la conformité des cellules (e.g., types de cellules, pièces présentes)
        for row in 0..<b.nbRows {
            for col in 0..<b.nbColumns {
                let cell = b.grid[row][col]
                // Vérifier le type de la cellule
                if row == 0 || row == 4 {
                    if col == 2 && cell.cellType != .den {
                        throw InvalidBoardError.badCellType(cell.cellType, row, col)
                    }
                } else if cell.cellType != .jungle {
                    throw InvalidBoardError.badCellType(cell.cellType, row, col)
                }
                
                // Vérifier la conformité des pièces
                if let piece = cell.piece {
                    if piece.owner != .player1 && piece.owner != .player2 {
                        throw InvalidBoardError.pieceWithNoOwner(piece)
                    }
                }
            }
        }
    }
    
    public func getMoves(board: Board, owner: Owner) -> [Move] {
        var moves = [Move]()
        for row in 0..<board.grid.count {
            for column in 0..<board.grid[row].count {
                if let piece = board.grid[row][column].piece, piece.owner == owner {
                    moves.append(contentsOf: getMoves(board: board, owner: owner, row: row, column: column))
                }
            }
        }
        return moves
    }

    public func getMoves(board: Board, owner: Owner, row: Int, column: Int) -> [Move] {
        var moves = [Move]()
        let directions = [(-1, 0), (1, 0), (0, -1), (0, 1)] // Haut, Bas, Gauche, Droite
        
        for (dRow, dCol) in directions {
            let newRow = row + dRow
            let newCol = column + dCol
            if newRow >= 0, newRow < board.grid.count, newCol >= 0, newCol < board.grid[0].count {
                let destinationCell = board.grid[newRow][newCol]
                // Autoriser le mouvement si la case de destination est vide ou contient une pièce ennemie
                if destinationCell.piece == nil || destinationCell.piece?.owner != owner {
                    moves.append(Move(owner: owner, rowOrigin: row, columnOrigin: column, rowDestination: newRow, columnDestination: newCol))
                }
            }
        }
        return moves
    }


        
    public func isMoveValid(board: Board, move: Move) -> Bool {
        // Assurez-vous que le mouvement est d'une case dans toutes les directions
        let deltaRow = abs(move.rowDestination - move.rowOrigin)
        let deltaColumn = abs(move.columnDestination - move.columnOrigin)
        if deltaRow > 1 || deltaColumn > 1 || (deltaRow == 0 && deltaColumn == 0) {
            return false
        }

        // Obtenez la cellule de destination
        let destinationCell = board.grid[move.rowDestination][move.columnDestination]
        
        // Vérifiez si la case de destination est occupée par une pièce alliée
        if let piece = destinationCell.piece, piece.owner == move.owner {
            return false
        }

        return true
    }
    
    public func isMoveValid(board: Board, row: Int, column: Int, rowArrived: Int, columnArrived: Int) -> Bool {
        // Création d'un objet Move à partir des coordonnées fournies
        let move = Move(owner: board.getCell(atRow: row, atColumn: column)?.piece?.owner ?? .noOne,
                        rowOrigin: row,
                        columnOrigin: column,
                        rowDestination: rowArrived,
                        columnDestination: columnArrived)
        
        // Utilisez la première version de isMoveValid pour vérifier la validité
        return isMoveValid(board: board, move: move)
    }

    
    public mutating func playedMove(move: Move, to board: inout Board) throws {
        // Assurez-vous que le mouvement est valide avant de continuer
        guard isMoveValid(board: board, move: move) else {
            throw GameError.invalidMove
        }

        // Obtenez la pièce à déplacer
        guard let movingPiece = board.getCell(atRow: move.rowOrigin, atColumn: move.columnOrigin)?.piece else {
            throw GameError.invalidMove // Ou une erreur plus spécifique
        }

        // Vérifiez s'il y a une pièce ennemie à la position de destination et la supprimez si c'est le cas
        if let destinationPiece = board.getCell(atRow: move.rowDestination, atColumn: move.columnDestination)?.piece {
            if destinationPiece.owner != movingPiece.owner {
                // La pièce ennemie est capturée et doit être supprimée
                _ = board.removePiece(atRow: move.rowDestination, andColumn: move.columnDestination)
            }
        }

        // Supprimez la pièce de sa position d'origine
        _ = board.removePiece(atRow: move.rowOrigin, andColumn: move.columnOrigin)

        // Placez la pièce à sa nouvelle position
        let insertionResult = board.insert(piece: movingPiece, atRow: move.rowDestination, andColumn: move.columnDestination)
        switch insertionResult {
        case .ok:
            print("Mouvement appliqué avec succès.")
        case .failed(let reason):
            print("Échec de l'application du mouvement : \(reason).")
            throw GameError.invalidMove
        }

        // Mettre à jour l'historique des mouvements ou d'autres états au besoin
        historic.append(move)
    }


    public func hasPlayerNoPieces(board: Board, player: Owner) -> Bool {
        for row in board.grid {
            for cell in row {
                if let piece = cell.piece, piece.owner == player {
                    return false
                }
            }
        }
        return true
    }


    public func isGameOver(board: Board, lastMove: Move) -> (Bool, Result) {
        let destinationCell = board.grid[lastMove.rowDestination][lastMove.columnDestination]
        var opponent: Owner
        if lastMove.owner == .player1 {
            opponent = .player2
        } else {
            opponent = .player1
        }

        // Vérifiez si la destination est la tanière de l'opposant
        if destinationCell.cellType == .den && destinationCell.initialOwner == opponent {
            return (true, .winner(lastMove.owner, .denReached))
        }

        // Vérifie si la tanière de l'adversaire a été atteinte
        if destinationCell.cellType == .den && destinationCell.initialOwner != lastMove.owner {
            return (true, .winner(lastMove.owner, .denReached))
        }

        // Vérifie si l'un des joueurs n'a plus de pièces
        if hasPlayerNoPieces(board: board, player: opponent) {
            return (true, .winner(lastMove.owner, .noMorePieces))
        } else if hasPlayerNoPieces(board: board, player: lastMove.owner) {
            return (true, .winner(opponent, .noMorePieces))
        }

        return (false, .notFinished)
    }


}
	
