//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 15/01/2024.
//

import Foundation

public struct Cell : CustomStringConvertible, Hashable, Equatable {
    public let cellType: CellType
    public var initialOwner: Owner
    public var piece: Piece?

    public init(ofType cellType: CellType, ownedBy initialOwner: Owner = .noOne, withPiece piece: Piece? = nil) {
        self.cellType = cellType
        self.initialOwner = initialOwner
        self.piece = piece
    }

    public var description: String {
        return "Cell(type: (cellType), owner: (initialOwner), piece: (pieceDescription))"
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(cellType)
        hasher.combine(initialOwner)
        hasher.combine(piece)
    }
}

