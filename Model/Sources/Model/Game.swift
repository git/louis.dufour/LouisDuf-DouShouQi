//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 05/02/2024.
//

import Foundation

public struct Game {
    public var rules: VerySimpleRules
    public var board: Board
    public var players: [Player]
    public var currentPlayerIndex: Int = 0

    public var onGameStart: (() -> Void)?
    public var onPlayerTurn: ((Player) -> Void)?
    public var onMoveSelected: ((Move) -> Void)?
    public var onInvalidMove: (() -> Void)?
    public var onBoardChanged: ((Board) -> Void)?
    public var onGameOver: ((Player?) -> Void)?
    public var onChooseMoveForHumanPlayer: ((Player, [Move]) -> Move?)?

    public init(rules: VerySimpleRules, player1: Player, player2: Player) {
        self.rules = rules
        self.board = VerySimpleRules.createBoard()
        self.players = [player1, player2]
    }

    public init(rules: VerySimpleRules, board: Board, players: [Player], currentPlayerIndex: Int) {
        self.rules = rules
        self.board = board
        self.players = players
        self.currentPlayerIndex = currentPlayerIndex
    }
    
    public mutating func start() {
        onGameStart?()
        onBoardChanged?(board)

        var gameEndResult: (finished: Bool, winner: Player?) = (false, nil)
        
        while !gameEndResult.finished {
            let currentPlayer = players[currentPlayerIndex]
            onPlayerTurn?(currentPlayer)

            let validMoves = rules.getMoves(board: board, owner: currentPlayer.id)
            var move: Move? = nil
            
            if let humanPlayer = currentPlayer as? HumanPlayer {
                move = onChooseMoveForHumanPlayer?(humanPlayer, validMoves)
            } else {
                move = currentPlayer.chooseMove(board: board, rules: rules)
            }

            if let selectedMove = move, rules.isMoveValid(board: board, move: selectedMove) {
                do {
                    try rules.playedMove(move: selectedMove, to: &board)
                    onMoveSelected?(selectedMove)
                    onBoardChanged?(board)

                    let result = rules.isGameOver(board: board, lastMove: selectedMove)
                    if result.0 {
                        gameEndResult = (true, currentPlayer)
                        onGameOver?(currentPlayer)
                    }
                } catch {
                    onInvalidMove?()
                }
            } else {
                onInvalidMove?()
            }

            currentPlayerIndex = (currentPlayerIndex + 1) % players.count
        }
    }
}
