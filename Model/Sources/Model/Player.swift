//
//  File.swift
//  
//
//  Created by Louis Dufour on 29/01/2024.
//

import Foundation

public class Player {
    public let id: Owner
    public let name: String

    public init?(name: String, id: Owner) {
        self.name = name
        self.id = id
    }

    public func chooseMove(board: Board, rules: Rules) -> Move? {
        fatalError("Cette méthode doit être implémentée par des sous-classes.")
    }
}
