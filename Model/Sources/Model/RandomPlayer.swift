//
//  File.swift
//  
//
//  Created by Louis Dufour on 29/01/2024.
//

import Foundation

public class RandomPlayer: Player {
    
    public init?(withName name: String, andId id:Owner)
    {
        super.init(name: name, id: id)
    }
    
    public override func chooseMove(board: Board, rules: Rules) -> Move? {
        let validMoves = rules.getMoves(board: board, owner: self.id)
        return validMoves.randomElement() // Sélectionne un mouvement aléatoire parmi les mouvements valides
    }
}
