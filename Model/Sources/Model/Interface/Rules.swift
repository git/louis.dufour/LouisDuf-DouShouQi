//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 22/01/2024.
//

import Foundation

public protocol Rules {
    var occurences: [Board: Int] { get set }
    var historic: [Move] { get set }

    static func createBoard() -> Board
    static func checkBoard( b: Board) throws
    func getNextPlayer() -> Owner

    func getMoves( board: Board, owner: Owner) -> [Move]
    func getMoves( board: Board, owner: Owner, row: Int, column: Int) -> [Move]
    
    func isMoveValid( board: Board, row: Int, column: Int, rowArrived: Int, columnArrived: Int) -> Bool
    func isMoveValid( board: Board, move: Move) -> Bool

    func isGameOver( board: Board, lastMove: Move) -> (Bool, Result)
    
    mutating func playedMove(move: Move, to board: inout Board) throws
}
