//
//  File.swift
//  
//
//  Created by Louis Dufour on 29/01/2024.
//

import Foundation

public class HumanPlayer: Player {
    private var inputMethod: (([Move]) -> Move?)

    public init?(name: String, id: Owner, inputMethod: @escaping ([Move]) -> Move?) {
        self.inputMethod = inputMethod
        super.init(name: name, id: id)
    }

    public override func chooseMove(board: Board, rules: Rules) -> Move? {
        let validMoves = rules.getMoves(board: board, owner: self.id)
        return inputMethod(validMoves)
    }
}
