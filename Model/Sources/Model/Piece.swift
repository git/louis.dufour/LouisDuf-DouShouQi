//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 15/01/2024.
//

import Foundation

public struct Piece : CustomStringConvertible, Equatable, Hashable, Codable{
    public let owner: Owner
    public let animal: Animal

    public init(withOwner owner: Owner, andAnimal animal: Animal) {
        self.owner = owner
        self.animal = animal
    }

    public var description: String {
        return "[(owner):(animal)]"
    }
}
