//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 15/01/2024.
//

import Foundation

public enum CellType : CustomStringConvertible, Codable {
    case unknown, jungle, water, trap, den
    
    public var description: String {
        switch self {
            case .unknown:
                return "unknown cell"
            case .jungle:
                return "jungle cell"
            case .water:
                return "water cell"
            case .trap:
                return "trap cell"
            case .den:
                return "den cell"
            }
        }
}

