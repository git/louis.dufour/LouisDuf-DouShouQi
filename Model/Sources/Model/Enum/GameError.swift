//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 22/01/2024.
//

import Foundation

public enum GameError: Error {
    case nextPlayerError
    case badPlayerId(String)
    case invalidMove
    case unknownError
}
