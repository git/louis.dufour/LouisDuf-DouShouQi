//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 15/01/2024.
//

import Foundation

public enum Animal : CustomStringConvertible, Codable { case rat, cat, dog, wolf, leopard, tiger, lion, elephant
    
    public var description: String {
        switch self {
        case .rat:
            return "Rat"
        case .cat:
            return "Cat"
        case .dog:
            return "Dog"
        case .wolf:
            return "Wolf"
        case .leopard:
            return "Leopard"
        case .tiger:
            return "Tiger"
        case .lion:
            return "Lion"
        case .elephant:
            return "Elephant"
        }
    }
}


