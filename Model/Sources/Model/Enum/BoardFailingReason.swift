//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 17/01/2024.
//

import Foundation

public enum BoardFailingReason {
    case outOfBounds
    case cellNotEmpty
    case cellEmpty
    case unknown
}
