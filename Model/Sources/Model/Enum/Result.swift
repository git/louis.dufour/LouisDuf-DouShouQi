//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 22/01/2024.
//

import Foundation

public enum Result {
    case notFinished
    case even
    case winner(Owner, WinningReason)
}
