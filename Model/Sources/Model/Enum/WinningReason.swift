//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 22/01/2024.
//

import Foundation

public enum WinningReason {
    case denReached
    case noMorePieces
    case noMovesLeft
    case tooManyOccurences
}
