//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 15/01/2024.
//

import Foundation

public enum Owner : CustomStringConvertible, Codable {
    case noOne, player1, player2

    public var description: String {
            switch self {
            case .player1:
                return "1"
            case .player2:
                return "2"
            case .noOne:
                return "x"
            }
        }
}


