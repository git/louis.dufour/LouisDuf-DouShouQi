import XCTest
@testable import Model
class BoardTests: XCTestCase {

    // Initialisez un Board de test avec une configuration de test
    var testBoard: Board!

    override func setUp() {
        super.setUp()
        do {
            testBoard = try Board(withGrid: initialTestConfiguration)
        } catch {
            XCTFail("Error setting up test: \(error)")
        }
    }

    override func tearDown() {
        // Nettoyez après chaque test si nécessaire
        testBoard = nil
        super.tearDown()
    }

    // Testez l'initialisateur de Board
    func testBoardInitializer() {
        XCTAssertNotNil(testBoard) // Vérifiez que le Board a été initialisé avec succès

        // Vérifiez que la taille du plateau correspond à la configuration initiale
        XCTAssertEqual(testBoard.nbRows, initialTestConfiguration.count)
        XCTAssertEqual(testBoard.nbColumns, initialTestConfiguration[0].count)
    }
    
    // Testez countPieces(of:)
    func testCountPiecesOfPlayer() {
        XCTAssertEqual(testBoard.countPieces(of: .player1), expectedPlayer1PieceCount)
        XCTAssertEqual(testBoard.countPieces(of: .player2), expectedPlayer2PieceCount)
    }

    // Testez countPieces()
    func testCountTotalPieces() {
        let (countPlayer1, countPlayer2) = testBoard.countPieces()
        XCTAssertEqual(countPlayer1, expectedPlayer1PieceCount)
        XCTAssertEqual(countPlayer2, expectedPlayer2PieceCount)
    }

    // Testez removePiece(atRow:andColumn:)
    func testRemovePiece() {
        let initialCell = testBoard.getCell(atRow: 0, atColumn: 0)
        XCTAssertNotNil(initialCell?.piece)
        let result = testBoard.removePiece(atRow: 0, andColumn: 0)
        XCTAssertEqual(result, .ok)

        // Vérifiez que la pièce a été supprimée correctement
        let removedCell = testBoard.getCell(atRow: 0, atColumn: 0)
        XCTAssertNil(removedCell?.piece)
    }
    
    // Testez insert(piece:atRow:andColumn:)
    func testInsertPiece() {
        // Supprimez toute pièce existante dans la case cible (si elle existe)
        let initialCell = testBoard.getCell(atRow: 0, atColumn: 0)
        if initialCell?.piece != nil {
            let removeResult = testBoard.removePiece(atRow: 0, andColumn: 0)
            XCTAssertEqual(removeResult, .ok)
        }

        // Insérez une nouvelle pièce dans la case
        let insertResult = testBoard.insert(piece: Piece(withOwner: .player1, andAnimal: .lion), atRow: 0, andColumn: 0)
        XCTAssertEqual(insertResult, .ok)

        // Vérifiez que la pièce a été insérée correctement
        let cell = testBoard.getCell(atRow: 0, atColumn: 0)
        XCTAssertNotNil(cell?.piece)
        XCTAssertEqual(cell?.piece?.owner, .player1)
        XCTAssertEqual(cell?.piece?.animal, .lion)
    }
    
    // Exemple de données de test pour la configuration initiale
    let initialTestConfiguration: [[Cell]] = [
        // Ligne 1
        [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .lion)),
         Cell(ofType: .jungle), Cell(ofType: .trap), Cell(ofType: .den), Cell(ofType: .trap),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .tiger))],

        // Ligne 2
        [Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .dog)),
         Cell(ofType: .jungle), Cell(ofType: .trap), Cell(ofType: .jungle),
         Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .cat)), Cell(ofType: .jungle)],

        // Ligne 3
        [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .rat)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .leopard)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .wolf)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .elephant))],

        // Lignes 4 à 7 (Eau et Jungle)
        [Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water),
         Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water), Cell(ofType: .jungle)],
        
        [Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water),
         Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water), Cell(ofType: .jungle)],

        [Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water),
         Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water), Cell(ofType: .jungle)],

        [Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water),
         Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water), Cell(ofType: .jungle)],

        // Ligne 8
        [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .elephant)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .wolf)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .leopard)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .rat))],

        // Ligne 9
        [Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .cat)),
         Cell(ofType: .jungle), Cell(ofType: .trap), Cell(ofType: .jungle),
         Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .dog)), Cell(ofType: .jungle)],

        // Ligne 10
        [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .tiger)),
         Cell(ofType: .jungle), Cell(ofType: .trap), Cell(ofType: .den), Cell(ofType: .trap),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .lion))]
    ]

    let expectedPlayer1PieceCount = 8 // Le nombre attendu de pièces pour le joueur 1
    let expectedPlayer2PieceCount = 8 // Le nombre attendu de pièces pour le joueur 2
}

class BoardPerformanceTests: XCTestCase {

    var testBoard: Board!

    override func setUp() {
        super.setUp()
        do {
            testBoard = try Board(withGrid: initialTestConfiguration)
        } catch {
            XCTFail("Error setting up test: \(error)")
        }
    }

    override func tearDown() {
        // Nettoyez après chaque test si nécessaire
        testBoard = nil
        super.tearDown()
    }

    // Test de performance pour countPieces(of:)
    func testPerformanceCountPiecesOfPlayer() {
        self.measure {
            for _ in 0..<1000 { // Exécutez le test 1000 fois pour mesurer les performances
                let _ = testBoard.countPieces(of: .player1)
            }
        }
    }

    // Test de performance pour countPieces()
    func testPerformanceCountTotalPieces() {
        self.measure {
            for _ in 0..<1000 { // Exécutez le test 1000 fois pour mesurer les performances
                let _ = testBoard.countPieces()
            }
        }
    }

    // Test de performance pour insert(piece:atRow:andColumn:)
    func testPerformanceInsertPiece() {
        let pieceToInsert = Piece(withOwner: .player1, andAnimal: .lion)
        self.measure {
            for _ in 0..<1000 { // Exécutez le test 1000 fois pour mesurer les performances
                let _ = testBoard.insert(piece: pieceToInsert, atRow: 0, andColumn: 0)
                // Nettoyez après chaque itération pour réinitialiser l'état du plateau
                let _ = testBoard.removePiece(atRow: 0, andColumn: 0)
            }
        }
    }

    // Test de performance pour removePiece(atRow:andColumn:)
    func testPerformanceRemovePiece() {
        let pieceToRemove = Piece(withOwner: .player1, andAnimal: .lion)
        let _ = testBoard.insert(piece: pieceToRemove, atRow: 0, andColumn: 0)
        self.measure {
            for _ in 0..<1000 { // Exécutez le test 1000 fois pour mesurer les performances
                let _ = testBoard.removePiece(atRow: 0, andColumn: 0)
                // Réinsérez la pièce après chaque itération pour réinitialiser l'état du plateau
                let _ = testBoard.insert(piece: pieceToRemove, atRow: 0, andColumn: 0)
            }
        }
    }
    
    // Test de performance pour l'initialisateur de Board
    func testPerformanceInitializeBoard() {
        self.measure {
            for _ in 0..<1000 { // Exécutez le test 1000 fois pour mesurer les performances
                _ = try! Board(withGrid: initialTestConfiguration)
            }
        }
    }

    // Exemple de données de test pour la configuration initiale
    let initialTestConfiguration: [[Cell]] = [
        // Ligne 1
        [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .lion)),
         Cell(ofType: .jungle), Cell(ofType: .trap), Cell(ofType: .den), Cell(ofType: .trap),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .tiger))],

        // Ligne 2
        [Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .dog)),
         Cell(ofType: .jungle), Cell(ofType: .trap), Cell(ofType: .jungle),
         Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .cat)), Cell(ofType: .jungle)],

        // Ligne 3
        [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .rat)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .leopard)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .wolf)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .elephant))],

        // Lignes 4 à 7 (Eau et Jungle)
        [Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water),
         Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water), Cell(ofType: .jungle)],
        
        [Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water),
         Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water), Cell(ofType: .jungle)],

        [Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water),
         Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water), Cell(ofType: .jungle)],

        [Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water),
         Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water), Cell(ofType: .jungle)],

        // Ligne 8
        [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .elephant)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .wolf)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .leopard)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .rat))],

        // Ligne 9
        [Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .cat)),
         Cell(ofType: .jungle), Cell(ofType: .trap), Cell(ofType: .jungle),
         Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .dog)), Cell(ofType: .jungle)],

        // Ligne 10
        [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .tiger)),
         Cell(ofType: .jungle), Cell(ofType: .trap), Cell(ofType: .den), Cell(ofType: .trap),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .lion))]
    ]
}
