import XCTest
@testable import Model
class VerySimpleRulesTests: XCTestCase {
    
    var rules: VerySimpleRules!
    var board: Board!

    override func setUp() {
        super.setUp()
        rules = VerySimpleRules()
        board = VerySimpleRules.createBoard()
    }

    override func tearDown() {
        rules = nil
        board = nil
        super.tearDown()
    }

    func testCreateBoard() {
        // Test que la fonction createBoard() retourne un plateau valide avec les dimensions attendues
        let board = VerySimpleRules.createBoard()
        XCTAssertEqual(board.nbRows, 5)
        XCTAssertEqual(board.nbColumns, 5)
    }

    func testGetNextPlayer() {
        // Teste que la fonction getNextPlayer() retourne le joueur suivant en fonction de l'historique des coups
        var rules = VerySimpleRules()
        rules.historic.append(Move(owner: .player1, rowOrigin: 0, columnOrigin: 0, rowDestination: 0, columnDestination: 1))
        XCTAssertEqual(rules.getNextPlayer(), .player2)

        rules.historic.append(Move(owner: .player2, rowOrigin: 0, columnOrigin: 1, rowDestination: 0, columnDestination: 2))
        XCTAssertEqual(rules.getNextPlayer(), .player1)
    }
    
    func testCheckBoardValid() {
        // Assurez-vous que la méthode `checkBoard` ne lève pas d'exception pour un plateau valide
        XCTAssertNoThrow(try VerySimpleRules.checkBoard(b: board))
    }

    func testCheckBoardInvalidDimensions() {
        // Création d'un plateau avec des dimensions invalides (4x4 au lieu de 5x5)
        var invalidCells = [[Cell]]()

        for _ in 0..<4 {
            var rowCells = [Cell]()
            for _ in 0..<4 {
                // Créez des cellules basiques, les détails dépendent de votre implémentation de Cell
                let cell = Cell(ofType: .jungle, ownedBy: .noOne, withPiece: nil)
                rowCells.append(cell)
            }
            invalidCells.append(rowCells)
        }

        do {
                let invalidBoard = try Board(withGrid: invalidCells)
            XCTAssertThrowsError(try VerySimpleRules.checkBoard(b: invalidBoard)) { error in
                XCTAssertTrue(error is Error)
                }
            } catch {
                XCTFail("Unexpected error while creating the board: \(error)")
            }
    }
   
    func testMoveValidation() {
          // Testez un mouvement valide
          let validMove = Move(owner: .player1, rowOrigin: 0, columnOrigin: 1, rowDestination: 1, columnDestination: 1)
          XCTAssertTrue(rules.isMoveValid(board: board, move: validMove), "Le mouvement valide n'a pas été reconnu comme tel")

          // Testez un mouvement invalide
          let invalidMove = Move(owner: .player1, rowOrigin: 0, columnOrigin: 1, rowDestination: 3, columnDestination: 3)
          XCTAssertFalse(rules.isMoveValid(board: board, move: invalidMove), "Le mouvement invalide a été reconnu comme valide")
      }
    
    func testGameProgression() {
        // Effectuer un mouvement valide
        let move = Move(owner: .player1, rowOrigin: 0, columnOrigin: 1, rowDestination: 1, columnDestination: 1)
        do {
            try rules.playedMove(move: move, to: &board)
        } catch {
            XCTFail("Le mouvement n'a pas été appliqué comme prévu: \(error)")
        }

        // Vérifier que l'historique des mouvements a été mis à jour
        XCTAssertEqual(rules.historic.count, 1, "L'historique des mouvements n'a pas été correctement mis à jour")

        // Vérifier le changement de joueur
        let nextPlayer = rules.getNextPlayer()
        XCTAssertEqual(nextPlayer, .player2, "Le joueur suivant n'a pas été correctement mis à jour")
    }

    func testGameProgressionWithInvalidMove() {
        // Essayer d'effectuer un mouvement invalide
        let invalidMove = Move(owner: .player1, rowOrigin: 0, columnOrigin: 1, rowDestination: 3, columnDestination: 3)
        XCTAssertThrowsError(try rules.playedMove(move: invalidMove, to: &board), "Un mouvement invalide a été accepté") { error in
            guard let gameError = error as? GameError else {
                XCTFail("Type d'erreur inattendu")
                return
            }
            switch gameError {
            case .invalidMove:
                // Succès attendu, le mouvement est invalide
                break
            default:
                XCTFail("Erreur inattendue pour un mouvement invalide")
            }
        }
    }
    
    func testGameOverConditions() {
        // Utiliser `isGameOver` pour vérifier l'état de fin de jeu
        let (isOver, result) = rules.isGameOver(board: board, lastMove: Move(owner: .player1, rowOrigin: 0, columnOrigin: 2, rowDestination: 4, columnDestination: 2))
        XCTAssertTrue(isOver, "Le jeu ne s'est pas terminé comme prévu")

        // Vérifier le résultat du jeu
        switch result {
        case .winner(let owner, let reason):
            XCTAssertEqual(owner, .player1, "Le mauvais joueur a été déclaré gagnant")
            // Ajouter des assertions spécifiques pour la raison de la victoire si nécessaire
        default:
            XCTFail("Résultat de fin de jeu inattendu")
        }
    }


    // Test pour obtenir une cellule spécifique du plateau
    func testGetCell() {
        let cell = board.getCell(atRow: 0, atColumn: 0)
        XCTAssertNotNil(cell, "La cellule n'a pas été correctement récupérée")
    }

    // Test pour obtenir les mouvements possibles pour un joueur
    func testGetMoves() {
        // Supposons que le joueur 1 ait des mouvements valides depuis sa position initiale
        let moves = rules.getMoves(board: board, owner: .player1)
        XCTAssertFalse(moves.isEmpty, "Aucun mouvement possible trouvé pour le joueur 1")
    }

    // Test pour vérifier le comportement de la fonction de mouvement
    func testPlayedMove() {
        // Assumer un mouvement valide
        let move = Move(owner: .player1, rowOrigin: 0, columnOrigin: 1, rowDestination: 1, columnDestination: 1)
        XCTAssertNoThrow(try rules.playedMove(move: move, to: &board), "Le mouvement valide a provoqué une erreur")

        // Vérifier l'état du plateau après le mouvement
        let destinationCell = board.getCell(atRow: 1, atColumn: 1)
        XCTAssertNotNil(destinationCell?.piece, "La pièce n'a pas été déplacée vers la nouvelle cellule")
        XCTAssertEqual(destinationCell?.piece?.owner, .player1, "La pièce déplacée n'appartient pas au joueur 1")
    }

    // Test pour valider un mouvement spécifique en utilisant des coordonnées
    func testIsMoveValidWithCoordinates() {
        // Valider un mouvement en utilisant des coordonnées directes
        XCTAssertTrue(rules.isMoveValid(board: board, row: 0, column: 1, rowArrived: 1, columnArrived: 1), "Le mouvement valide n'a pas été reconnu")
        
        // Tester un mouvement invalide (par exemple, trop loin)
        XCTAssertFalse(rules.isMoveValid(board: board, row: 0, column: 0, rowArrived: 3, columnArrived: 3), "Un mouvement invalide a été reconnu comme valide")
    }
}
