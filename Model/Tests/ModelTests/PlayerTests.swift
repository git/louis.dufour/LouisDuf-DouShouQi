//
//  File.swift
//  
//
//  Created by Louis DUFOUR on 31/01/2024.
//

import XCTest
@testable import Model 

class PlayerTests: XCTestCase {
    
    var rules: VerySimpleRules!
    var board: Board!

    override func setUp() {
        super.setUp()
        rules = VerySimpleRules()
        board = VerySimpleRules.createBoard()
    }

    override func tearDown() {
        rules = nil
        board = nil
        super.tearDown()
    }
    
    func testRandomPlayerChooseMove() {
        // Assurez-vous que l'initialisation de RandomPlayer correspond à votre implémentation
        let randomPlayer = RandomPlayer(withName: "Random Player", andId: .player1)
        XCTAssertNotNil(randomPlayer, "RandomPlayer n'a pas été correctement initialisé")
        
        // Testez chooseMove avec le board et les règles réels
        let move = randomPlayer?.chooseMove(board: board, rules: rules)
        XCTAssertNotNil(move, "RandomPlayer n'a pas choisi de mouvement")
    }

    func testHumanPlayerChooseMove() {
        // Assurez-vous que l'initialisation de HumanPlayer correspond à votre implémentation
        let inputMethod: ([Move]) -> Move? = { moves in moves.first }
        let humanPlayer = HumanPlayer(name: "Human Player", id: .player1, inputMethod: inputMethod)
        XCTAssertNotNil(humanPlayer, "HumanPlayer n'a pas été correctement initialisé")
        
        // Testez chooseMove avec le board et les règles réels
        let move = humanPlayer?.chooseMove(board: board, rules: rules)
        XCTAssertNotNil(move, "HumanPlayer n'a pas choisi de mouvement")
    }
}
