//
//  File.swift
//  
//
//  Created by Louis Dufour on 16/02/2024.
//

import Foundation
import Model
extension Player {
    func toPlayerData() -> PlayerData {
        return PlayerData(id: self.id, name: self.name, type: type(of: self) == HumanPlayer.self ? "HumanPlayer" : "RandomPlayer")
    }
}
