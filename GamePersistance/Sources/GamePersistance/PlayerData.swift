//
//  File.swift
//  
//
//  Created by Louis Dufour on 16/02/2024.
//

import Foundation
import Model

struct PlayerData: Codable {
    let id: Owner
    let name: String
    let type: String // Pour distinguer HumanPlayer de RandomPlayer
}
