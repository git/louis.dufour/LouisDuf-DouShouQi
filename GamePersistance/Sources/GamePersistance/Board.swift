//
//  File.swift
//  
//
//  Created by Louis Dufour on 16/02/2024.
//

import Foundation
import Model

extension Board: Codable {
    private enum CodingKeys: String, CodingKey {
        case nbRows, nbColumns, grid
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let decodedGrid = try container.decode([[Cell]].self, forKey: .grid)
        
        try self.init(withGrid: decodedGrid)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(nbRows, forKey: .nbRows)
        try container.encode(nbColumns, forKey: .nbColumns)
        try container.encode(grid, forKey: .grid)
    }
}
