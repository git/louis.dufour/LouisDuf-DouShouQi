//
//  File.swift
//  
//
//  Created by Louis Dufour on 16/02/2024.
//

import Foundation
import Model

extension HumanPlayer {
    static func from(data: PlayerData) -> HumanPlayer? {
        guard data.type == "HumanPlayer" else { return nil }
        return HumanPlayer(name: data.name, id: data.id, inputMethod: { _ in nil }) 
    }
}
