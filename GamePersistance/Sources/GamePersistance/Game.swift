//
//  File.swift
//  
//
//  Created by Louis Dufour on 16/02/2024.
//

import Foundation
import Model

extension Game: Codable {
    enum CodingKeys: String, CodingKey {
        case rules, board, players, currentPlayerIndex
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(rules, forKey: .rules)
        try container.encode(board, forKey: .board)
        try container.encode(players.map { $0.toPlayerData() }, forKey: .players)
        try container.encode(currentPlayerIndex, forKey: .currentPlayerIndex)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        // Décodez directement les propriétés.
        let rules = try container.decode(VerySimpleRules.self, forKey: .rules)
        let board = try container.decode(Board.self, forKey: .board)
        let currentPlayerIndex = try container.decode(Int.self, forKey: .currentPlayerIndex)
        
        // Décodez chaque PlayerData en une instance de Player.
        let playersData = try container.decode([PlayerData].self, forKey: .players)
        let players = try playersData.map { data -> Player in
            switch data.type {
            case "HumanPlayer":
                guard let humanPlayer = HumanPlayer.from(data: data) else {
                    throw DecodingError.dataCorruptedError(forKey: .players,
                                                           in: container,
                                                           debugDescription: "Cannot decode HumanPlayer")
                }
                return humanPlayer
            case "RandomPlayer":
                guard let randomPlayer = RandomPlayer.from(data: data) else {
                    throw DecodingError.dataCorruptedError(forKey: .players,
                                                           in: container,
                                                           debugDescription: "Cannot decode RandomPlayer")
                }
                return randomPlayer
            default:
                throw DecodingError.dataCorruptedError(forKey: .players,
                                                       in: container,
                                                       debugDescription: "Unrecognized player type")
            }
        }

        self.init(rules: rules, board: board, players: players, currentPlayerIndex: currentPlayerIndex)
    }
}


