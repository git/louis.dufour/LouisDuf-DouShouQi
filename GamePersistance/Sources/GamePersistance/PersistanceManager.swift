//
//  File.swift
//  
//
//  Created by Louis Dufour on 16/02/2024.
//

import Foundation
import Model

public struct PersistenceManager {
    
    public static let shared = PersistenceManager()

    private let fileName = "savedGame.json"

    public func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

    public func saveGame(game: Game) {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        do {
            let data = try encoder.encode(game)
            let url = getDocumentsDirectory().appendingPathComponent(fileName)
            try data.write(to: url)
            print("Game saved successfully.")
        } catch {
            print("Failed to save game: \(error)")
        }
    }

    public func loadGame() -> Game? {
        let url = getDocumentsDirectory().appendingPathComponent(fileName)
        do {
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let game = try decoder.decode(Game.self, from: data)
            print("Game loaded successfully.")
            return game
        } catch {
            print("Failed to load game: \(error)")
            return nil
        }
    }
}
