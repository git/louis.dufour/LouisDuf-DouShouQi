//
//  File.swift
//  
//
//  Created by Louis Dufour on 16/02/2024.
//

import Foundation
import Model
extension RandomPlayer {
    static func from(data: PlayerData) -> RandomPlayer? {
        guard data.type == "RandomPlayer" else { return nil }
        return RandomPlayer(withName: data.name, andId: data.id)
    }
}
