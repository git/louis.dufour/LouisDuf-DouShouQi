//
//  File.swift
//  
//
//  Created by Louis Dufour on 16/02/2024.
//

import Foundation
import Model

extension VerySimpleRules: Codable {
    enum CodingKeys: String, CodingKey {
        case occurences, historic
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(occurences, forKey: .occurences)
        try container.encode(historic, forKey: .historic)
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let decodedOccurences = try container.decode(Dictionary<Board, Int>.self, forKey: .occurences)
        let decodedHistoric = try container.decode([Move].self, forKey: .historic)
        
        self.init(occurences: decodedOccurences, historic: decodedHistoric)
    }
}
