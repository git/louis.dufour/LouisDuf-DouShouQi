//
//  main.swift
//  ProjectDouShouQi
//
//  Created by Louis DUFOUR on 15/01/2024.
//

import Foundation
import Model
import Extension
import GamePersistance

func olMain(){
    
    let initialBoardConfiguration: [[Cell]] = [
        // Ligne 1
        [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .lion)),
         Cell(ofType: .jungle), Cell(ofType: .trap), Cell(ofType: .den), Cell(ofType: .trap),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .tiger))],
        
        // Ligne 2
        [Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .dog)),
         Cell(ofType: .jungle), Cell(ofType: .trap), Cell(ofType: .jungle),
         Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .cat)), Cell(ofType: .jungle)],
        
        // Ligne 3
        [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .rat)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .leopard)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .wolf)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player1, andAnimal: .elephant))],
        
        // Lignes 4 à 7 (Eau et Jungle)
        [Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water),
         Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water), Cell(ofType: .jungle)],
        
        [Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water),
         Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water), Cell(ofType: .jungle)],
        
        [Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water),
         Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water), Cell(ofType: .jungle)],
        
        [Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water),
         Cell(ofType: .jungle), Cell(ofType: .water), Cell(ofType: .water), Cell(ofType: .jungle)],
        
        // Ligne 8
        [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .elephant)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .wolf)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .leopard)),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .rat))],
        
        // Ligne 9
        [Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .cat)),
         Cell(ofType: .jungle), Cell(ofType: .trap), Cell(ofType: .jungle),
         Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .dog)), Cell(ofType: .jungle)],
        
        // Ligne 10
        [Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .tiger)),
         Cell(ofType: .jungle), Cell(ofType: .trap), Cell(ofType: .den), Cell(ofType: .trap),
         Cell(ofType: .jungle), Cell(ofType: .jungle, withPiece: Piece(withOwner: .player2, andAnimal: .lion))]
    ]
    
    do {
        let board = try Board(withGrid: initialBoardConfiguration)
        // Afficher le plateau de jeu
        print(board)
        } catch {
            print("Erreur lors de l'initialisation du plateau de jeu: \(error)")
        }

    do {
        var board = try Board(withGrid: initialBoardConfiguration)
        print("Plateau initial:")
        print(board) // Affichez l'état initial du plateau
          
        print("Plateau initial:")
        print(board) // Affichez l'état initial du plateau
        
        // Testez countPieces(of:)
        let player1PieceCount = board.countPieces(of: .player1)
        print("Nombre de pièces pour le Joueur 1: \(player1PieceCount)")
        
        let player2PieceCount = board.countPieces(of: .player2)
        print("Nombre de pièces pour le Joueur 2: \(player2PieceCount)")
        
        // Testez countPieces()
        let (countPlayer1, countPlayer2) = board.countPieces()
        print("Comptage total - Joueur 1: \(countPlayer1), Joueur 2: \(countPlayer2)")
        
        // Testez removePiece(atRow:andColumn:)
        let removeResult = board.removePiece(atRow: 0, andColumn: 0)
        print("Résultat de la suppression : \(removeResult)")
        print(board) // Affichez le plateau après suppression
        
        // Testez insert(piece:atRow:andColumn:)
        let insertResult = board.insert(piece: Piece(withOwner: .player1, andAnimal: .lion), atRow: 0, andColumn: 0)
        print("Résultat de l'insertion : \(insertResult)")
        print(board) // Affichez le plateau après insertion
    } catch {
         print("Erreur lors de l'initialisation du plateau de jeu: \(error)")
     }
}

func setupHumanVsComputer() -> (Player, Player) {
    guard let randomPlayer = RandomPlayer(withName: "Random Player 1", andId: .player1),
          let humanPlayer = HumanPlayer(name: "Human Player 2", id: .player2, inputMethod: { validMoves in
              return validMoves.first
          }) else {
        fatalError("Failed to initialize players")
    }
    return (randomPlayer, humanPlayer)
}

func setupHumanVsHuman() -> (Player, Player) {
    guard let humanPlayer1 = HumanPlayer(name: "Player 1", id: .player1, inputMethod: { _ in nil }),
          let humanPlayer2 = HumanPlayer(name: "Player 2", id: .player2, inputMethod: { _ in nil }) else {
        fatalError("Failed to initialize players")
    }
    return (humanPlayer1, humanPlayer2)
}

func main() {
   print("Sélectionnez une option :")
   print("1: Charger une partie sauvegardée")
   print("2: Nouvelle partie Humain contre Ordinateur")
   print("3: Nouvelle partie Humain contre Humain")

   guard let userInput = readLine(), let userChoice = Int(userInput) else {
       fatalError("Entrée invalide")
   }

   var game: Game

    switch userChoice {
       case 1:
           if let loadedGame = PersistenceManager.shared.loadGame() {
               game = loadedGame
               print("Partie chargée avec succès.")
           } else {
               print("Aucune partie sauvegardée trouvée, début d'une nouvelle partie.")
               let players = setupHumanVsComputer()
               game = Game(rules: VerySimpleRules(), player1: players.0, player2: players.1)
           }
       case 2:
           let players = setupHumanVsComputer()
           game = Game(rules: VerySimpleRules(), player1: players.0, player2: players.1)
       case 3:
           let players = setupHumanVsHuman()
           game = Game(rules: VerySimpleRules(), player1: players.0, player2: players.1)
       default:
           fatalError("Option non valide")
   }

    // Configuration des callbacks pour la gestion des événements de jeu
    game.onGameStart = {
        print("**************************************")
        print("          ==>> GAME STARTS! <<==      ")
        print("**************************************")
    }

    game.onPlayerTurn = { player in
        print("**************************************")
        print("\(player.name), it's your turn!")
        print("**************************************")
    }

    game.onMoveSelected = { move in
        print("Move selected: \(move)")
    }

    game.onInvalidMove = {
        print("Invalid move! Try again.")
    }

    game.onBoardChanged = { board in
        print("Board updated:\n\(board)")
    }

    game.onGameOver = { winner in
        if let winner = winner {
            print("**************************************")
            print("Game Over!!! And the winner is... \(winner.name)!")
            print("**************************************")
        } else {
            print("Game Over!!! It's a draw.")
        }
    }

    game.onChooseMoveForHumanPlayer = { player, validMoves in
        print("Mouvements possibles pour \(player.name):")
        for (index, move) in validMoves.enumerated() {
            print("\(index + 1): \(move)")
        }
        print("Veuillez choisir un mouvement (entrez le numéro):")
        
        while let input = readLine(), let choice = Int(input), choice > 0, choice <= validMoves.count {
            return validMoves[choice - 1]
        }
        return nil
    }

    game.start()
}

main()


print("Vous voulez refaire une partie ?")
print("1: oui")
print("2: non")

guard let userInput = readLine(), let userChoice = Int(userInput) else {
    fatalError("Entrée invalide")
}

switch userChoice {
case 1:
    main()
case 2:
    exit(0)
default:
    fatalError("Option non valide")
}
