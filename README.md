
# Projet DouShouQi en Swift
Le projet DouShouQi en Swift est une implémentation console du jeu de plateau DouShouQi.

## Prérequis
- **Système d'exploitation**: macOS Ventura 13.1
- **Logiciel**: Xcode version 14.2

## Cas d'erreur connus
La persistance marche partiellement 

*(lorsqu'on appel save dans main cela plante)*

## Correction
Merci de prendre en compte la correction à partir de ce dernier commit: 6c02ae9efb85638dc1b00285d2e4148626420c00

*(j'ai essayer d'aller au bout du projet)*

## Installation et Configuration
### Étape 1 : Clonage du projet
```
git clone <lien-du-projet>
```

### Étape 2 : Ouvrir le projet dans Xcode
- Ouvrez Xcode.
- Cliquez sur "File" (Fichier) dans la barre de menu.
- Sélectionnez "Open" (Ouvrir).
- Naviguez jusqu'au répertoire où vous avez cloné le projet et ouvrez le fichier `.xcodeproj`.

### Étape 3 : Compilation et exécution
- Appuyez sur le bouton de lecture (triangle vert) dans Xcode pour compiler et exécuter le projet.
